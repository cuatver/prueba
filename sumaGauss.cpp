#include <iostream>
#include <omp.h>

#define N 100
using namespace std;

int main() {
  int a[N];     // crea un arreglo de longitud N
  int sum = 0;  // para guardar la suma
  int np = 100; // numero de pruebas

  // inicializa el arreglo con los numeros 0,1,2,...,N-1
  for(int i=0; i < N; i++){
    a[i] = i;
  }


  for (int p=0; p<np; p++){
    // creamos la región paralela
    #pragma omp parallel shared(sum)
    {
      // cálculo de la suma
      #pragma omp for schedule(static,1) 
      for (int i=0; i< N; i++) {
        sum += a[i]; 
      }
    }

    if(sum != N*(N-1)/2){
      cout << "Los resultados difieren en la prueba numero " << p+1 << endl;
      cout << "\t" << sum << "\t" << N*(N-1)/2 << endl;
      return 0;
    }

    sum = 0;
  }

  cout << "En " << np << " pruebas, las sumas siempre coincidieron." << endl;
}
