#include <iostream>
#include <omp.h>
using namespace std;

int main(){
  int N = 32000000;
  int *vectorA = new int[N];
  int *vectorB = new int[N];
  int *vectorS = new int[N];
  int i;
  int id;
  int nH[] = {1,2,4,8};
  double tiempo;

  for (int h=0; h<4; h++){
    omp_set_num_threads(nH[h]);

    tiempo = omp_get_wtime();
    # pragma omp parallel for private(i)
    for(i = 0; i<N; i++){
      vectorA[i] = 10*nH[h];
      vectorB[i] = 20*nH[h];
    }

    # pragma omp parallel for private(i)
    for(i = 0; i<N; i++){
      vectorS[i] = vectorA[i] + vectorB[i];

    }
    tiempo = omp_get_wtime()-tiempo;

    cout << vectorS[0] << " , " << vectorS[N-1] << endl;
    cout << "Tiempo de ejecución: " << tiempo << endl;

    # pragma omp parallel
    {
      id = omp_get_thread_num();
      if(id == 0){
        cout << "Se usaron: " << omp_get_num_threads() << " de " << omp_get_num_procs() << " procesadores disponibles \n\n" << endl;
      }
    }
  }

  delete[] vectorA;
  delete[] vectorB;
  delete[] vectorS;
}