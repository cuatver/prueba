/*
Multiplicación de matrices
*/
#include <iostream>
#include <omp.h>

#define N 1500
using namespace std;

int main() {
  // CAmbiar a memoria dinamica
  double *A = new double[N*N];
  double *B = new double[N*N];
  double *C = new double[N*N];
  double tmp;   // variable temporal
  int iN;

  // llenar matrices
  for(int i=0; i<N; i++){
    for(int j=0; j<N; j++){
      A[i*N+j] = 10.0;
      B[j*N+i] = 0.4;
    }
  }

  // multiplicacion
  #pragma omp parallel for private(tmp,iN) shared(A, B, C)
    for(int i=0; i<N; i++){
      for(int j=0; j<N; j++){
        tmp = 0;
        iN = i*N;
        for (int k=0; k<N; k++){
          tmp += A[iN+k]*B[j*N+k];
        }
        C[iN+j] = tmp;
      }
    }

  /*
  // mostrar resultado
  for(int i=0; i<N; i++){
    for(int j=0; j<N; j++){
      cout << C[i][j] << " ";
    }
    cout << endl;
  }
  */
  cout << C[0] << " " << C[(N-1)*N+N-1] << endl;

  delete[] A;
  delete[] B;
  delete[] C;
}
