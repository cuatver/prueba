/*
Simulación de un dado con paralelización
y la función rand_r()
*/
#include <iostream>
#include <omp.h>
#include <time.h>       /* time */
#include <stdlib.h>

using namespace std;

int Dado(double u){
  return ( (int) 6*u ) + 1;
}

int main(int argc, char const *argv[])
{
  /* code */
  int N = 32000000;
  double x, y;
  int exitos = 0;

  for (int i = 0; i < N; i++){
    x = 2* ( rand() / ( (double) RAND_MAX ) ) -1;
    y = 2* ( rand() / ( (double) RAND_MAX ) ) -1;
    if( x*x + y*y < 1.0){
      exitos += 1;
    }
  }

  cout << "\nAproximación: " << 4.0*exitos/(double) N << endl;
  return 0;
}