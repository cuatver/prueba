/*
Reparando el código de sumaGauss
Segunda versión: uso de arreglos (forma más general)
*/
#include <iostream>
#include <omp.h>

#define N 100
using namespace std;

int main() {
  int a[N];       // crea un arreglo de longitud N
  int sum = 0;    // para guardar la suma
  int nh;         // número de hilos
  int id;         // id de cada hilo
  int *local_sum; // arreglo de sumas parciales
  int np = 2000;   // numero de pruebas

  // obtenemos el número de hilos
  nh =  omp_get_num_procs();
  // creamos el arreglo de tamaño nh
  local_sum = new int[nh];

  // inicializa el arreglo con los numeros 0,1,2,...,N-1
  for(int i=0; i < N; i++){
    a[i] = i;
  }

  for (int p=0; p<np; p++){
    // creamos la región paralela
    #pragma omp parallel shared(local_sum) private(id)
    {
      id = omp_get_thread_num();
      local_sum[id] = 0;

      // cálculo de la suma
      #pragma omp parallel for schedule(static,1) 
      for (int i=0; i< N; i++) {
        local_sum[id] += a[i]; 
      }
    }

    for(int i = 0; i<nh; ++i){
      sum += local_sum[i];
    }

    if(sum != N*(N-1)/2){
      cout << "Los resultados difieren en la prueba numero " << p+1 << endl;
      cout << "\t" << sum << "\t" << N*(N-1)/2 << endl;
      return 0;
    }

    sum = 0;
  }

  cout << "En " << np << " pruebas, las sumas siempre coincidieron." << endl;
  delete[] local_sum;
}
