#include <iostream>
#include <omp.h>

#define N 100
using namespace std;

int main() {
  int a[N];       // crea un arreglo de longitud N

  // inicializa el arreglo con los numeros 0,1,2,...,N-1
  #pragma omp parallel for
    for(int i=0; i < N; i++){
      a[i] = omp_get_thread_num();
    }

  cout << "Hilos asignados a cada arreglo" << endl;
  for(int i=0; i < N; i++){
    cout << a[i] <<  " " ;
  }
  cout << endl;

  cout << "\n\nIntento de impresión con hilos" << endl;
  // inicializa el arreglo con los numeros 0,1,2,...,N-1
  #pragma omp parallel for
    for(int i=0; i < N; i++){
      cout << a[i] <<  " " ;
    }

  cout << endl;
}
