#include <iostream>
#include <omp.h>
#include <unistd.h>

using namespace std;

int main(){
  int N = 10000;   // tamaño de los vectores

  // Vectores
  double *vectorA = new double[N];
  double *vectorB = new double[N];
  double producto_punto = 0;

  int id;   // id de cada hilo
  int NumHilos[] = {1,2,3};   // hilos que vamos a usar

  double tiempo;  // para medir el tiempo

  int i,h;    // para los ciclos for

  for(h=0; h<3; h++){
    omp_set_num_threads( NumHilos[h] );

    tiempo = omp_get_wtime();
    # pragma omp parallel shared(producto_punto)
    {
      # pragma omp for private(i) nowait
      for(i = 0; i<N; i++){
        vectorA[i] = i;
        vectorB[i] = 1.0;
      }

      # pragma omp for private(i) nowait
      for(i = 0; i<N; i++){
        producto_punto += vectorA[i] * vectorB[i];
      }
      
      id = omp_get_thread_num();
      if(id == 0){
        cout << "Se usaron: " << omp_get_num_threads( ) << " de " << omp_get_num_procs() << " procesadores disponibles\n" << endl;
      }
    }
    
    tiempo = omp_get_wtime()-tiempo;
    cout << producto_punto << " , " << N*(N-1)/2.0 << endl;
    cout << "Tiempo de ejecución: " << tiempo << endl;
    producto_punto = 0;
  }

  delete[] vectorA;
  delete[] vectorB;
}