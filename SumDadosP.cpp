/*
Simulación de un dado con paralelización
y la función rand_r()
*/
#include <iostream>
#include <omp.h>
#include <time.h>       /* time */
#include <stdlib.h>

using namespace std;

int Dado(double u){
  return ( (int) 6*u ) + 1;
}

int main(int argc, char const *argv[])
{
  /* code */
  int dado1, dado2, dado3;
  int suma;
  double esperado;
  int N = 32000000;
  int nH[] = {1,2,4,8}; // numero de hilos
  double tiempo;        // tiempo de ejecucion
  int id;
  unsigned int semilla;

  for (int h=0; h<4; h++){
    suma = 0;
    omp_set_num_threads(nH[h]);
    tiempo = omp_get_wtime();

    #pragma omp parallel private(semilla, id)
    {
      id = omp_get_thread_num();
      semilla = (id+1)*10;
      //semilla = 2021;
      srand(semilla);

      #pragma omp for reduction(+:suma) schedule(static,1)
        for(int i=0; i<N; i++){
          // genramos los dados
          dado1 = Dado( rand_r(&semilla)/( (double) RAND_MAX ) );
          dado2 = Dado( rand_r(&semilla)/( (double) RAND_MAX ) );
          dado3 = Dado( rand_r(&semilla)/( (double) RAND_MAX ) );

          suma += dado1 + dado2 + dado3;  // suma de los dados
        }
    }    
    
    esperado = suma/( (double) N );
    tiempo = omp_get_wtime()-tiempo;

    #pragma omp parallel private(id)
    {
      id = omp_get_thread_num();
      if(id == 0){
        cout << "Se usaron: " << omp_get_num_threads() << " de " << omp_get_num_procs() << " procesadores disponibles" << endl;
      }
    }
    cout << "El valor esperado de la suma de los dados es: ";
    cout << esperado << endl;
    cout << "Tiempo de ejecución: " << tiempo << endl << endl;
  }

/*
   semilla = 2021;
   suma = 0;
    for(int i=0; i<40; i++){
      // genramos los dados
      dado1 = Dado( rand_r(&semilla)/( (double) RAND_MAX ) );
      dado2 = Dado( rand_r(&semilla)/( (double) RAND_MAX ) );
      dado3 = Dado( rand_r(&semilla)/( (double) RAND_MAX ) );

      suma += dado1 + dado2 + dado3;  // suma de los dados
    }
    esperado = suma/( (double) 40 );
    cout << esperado << endl;
*/
  return 0;
}