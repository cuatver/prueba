/*
Simulación de un dado con paralelización mal hecha
*/
#include <iostream>
#include <omp.h>
#include <time.h>       /* time */

using namespace std;

int Dado(double u){
  return ( (int) 6*u ) + 1;
}

int main(int argc, char const *argv[])
{
  /* code */
  int dado1, dado2, dado3;
  int suma;
  double esperado;
  int N = 32000000;
  int nH[] = {1,2,4,8}; // numero de hilos
  double tiempo;        // tiempo de ejecucion
  int id;


  for (int h=0; h<4; h++){
    omp_set_num_threads(nH[h]);
    tiempo = omp_get_wtime();

    //srand(time(NULL)); // incializamos la semilla
    srand( 2020 ); // misma secuencia de aleatorios

    #pragma omp parallel for reduction(+:suma)
    for(int i=0; i<N; i++){
      // genramos los dados
      dado1 = Dado( rand()/( (double) RAND_MAX ) );
      dado2 = Dado( rand()/( (double) RAND_MAX ) );
      dado3 = Dado( rand()/( (double) RAND_MAX ) );

      suma += dado1 + dado2 + dado3;  // suma de los dados
    }
    esperado = suma/( (double) N );
    tiempo = omp_get_wtime()-tiempo;

    #pragma omp parallel private(id)
    {
      id = omp_get_thread_num();
      if(id == 0){
        cout << "Se usaron: " << omp_get_num_threads() << " de " << omp_get_num_procs() << " procesadores disponibles" << endl;
      }
    }
    cout << "El valor esperado de la suma de los dados es: ";
    cout << esperado << endl;
    cout << "Tiempo de ejecución: " << tiempo << endl << endl;
    suma = 0;
  }
  return 0;
}